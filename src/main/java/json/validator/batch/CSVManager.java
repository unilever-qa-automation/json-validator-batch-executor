package json.validator.batch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;

public class CSVManager {
	protected static final Logger logger = LoggerFactory.getLogger(CSVManager.class);

	public static void main(String... s) {
		String location = System.getProperty("CSVLocation");
		List<CSVRecord> records = readCSV(location);
		Map<String, String> map = new HashMap<>();
		for (CSVRecord record : records) {
			map.put("SiteAddress", record.get("URL"));
			map.put("Username", record.get("Username"));
			map.put("Password", record.get("Password"));
			map.put("Password", record.get("Password"));
			map.put("Schema", record.get("Schema"));
			map.put("SinglePage", "No");
			map.put("CrawlerConfigFileWeb", getConfig(record.get("regex")));
			try {
				JenkinsServer js = new JenkinsServer(URI.create("http://10.207.16.9/jenkins/"));
				JobWithDetails job = js.getJob("JSON-Validator");
				job.build(map, true);
				Thread.sleep(1000);
			} catch (Exception e) {
				LoggerFactory.getLogger(CSVManager.class).error("Build not started", e);
			}

		}
		File f = new File("data");
		FileUtils.deleteQuietly(f);
	}

	private static String getConfig(String string) {
		string = string.replaceAll("http://", "").replaceAll("https://", "");
		String regex = "^(http://" + string + ")|(https://" + string + ")";
		String PROPERTIES_LOC = HelperUtils.getResource("Config.properties").getAbsolutePath();
		Properties PROPERTIES = new Properties();
		try {
			Scanner in = new Scanner(new FileInputStream(new File(PROPERTIES_LOC)));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			while (in.hasNext()) {
				out.write(in.nextLine().replace("\\", "\\\\").getBytes());
				out.write("\n".getBytes());
			}
			InputStream is = new ByteArrayInputStream(out.toByteArray());
			PROPERTIES.load(is);
			in.close();
		} catch (IOException e) {
			LoggerFactory.getLogger(CSVManager.class).error("Error in loading config file", e);
		}
		PROPERTIES.setProperty("crawler.domainRegex", regex);
		File f = new File("data" + File.separator + HelperUtils.generateUniqueString());
		f.mkdirs();
		File pro = new File(f, "Config.properties");
		List<String> list = new ArrayList<>();
		for (String key : PROPERTIES.stringPropertyNames()) {
			list.add(key + "=" + PROPERTIES.getProperty(key));
		}
		try {
			FileUtils.writeLines(pro, "UTF-8", list);
		} catch (IOException e) {
			LoggerFactory.getLogger(CSVManager.class).error("Error in loading config file", e);
		}
		System.out.println(pro.getAbsolutePath());
		return pro.getAbsolutePath();
	}

	/**
	 * Method to read CSV File.
	 *
	 * @return List<CSVRecord> List of CSV Records
	 *
	 ***/

	public static List<CSVRecord> readCSV(String csvPath) {
		try {
			CSVParser parser = CSVParser.parse(new File(csvPath), StandardCharsets.UTF_8, CSVFormat.EXCEL.withHeader());
			try {
				return parser.getRecords();
			} finally {
				parser.close();
			}
		} catch (FileNotFoundException ex) {
			LoggerFactory.getLogger(HelperUtils.class).debug("DEBUG", ex);
		} catch (IOException ex) {
			LoggerFactory.getLogger(HelperUtils.class).debug("DEBUG", ex);
		}
		return null;
	}
}
